Demo that works on different platforms (WAS / Open Liberty / Podman / Docker / OpenShift)


RUN On WAS 9
Running the image by using the default values

docker run --name was-server -h was-server -p 9043:9043 -p 9443:9443 -p 9080:9080 -d ibmcom/websphere-traditional:latest

Retrieving the password
The admin console user id is defaulted to wsadmin and the initial wsadmin user password is in /tmp/PASSWORD

docker exec was-server cat /tmp/PASSWORD


Checking the logs
docker logs -f --tail=all <container-name>
Example:

docker logs -f --tail=all was-server


Stopping the Application Server gracefully
docker stop --time=<timeout> <container-name>
Example:

docker stop --time=60 was-server   


wsadmin
y95eIiyz


http://localhost:9080/

http://localhost:9080/javaee-cafe/
=========================================================================================================================
Run local with liberty

mvn clean install liberty:dev

http://localhost:9085/
=========================================================================================================================
With podman

# Build and tag application image. This will cause podman to pull the necessary Open Liberty base images.
podman build -t javaee-cafe-simple:1.0.0 --pull .

podman run -it --rm -p 9080:9080 javaee-cafe-simple:1.0.0

----------------------------------------------------------------------------------------------------------------------------------
=========================================================================================================================
ocp

oc login --token=xxx --server=https://server:6443

---------------------------------------------------------
oc project demo-poc


a. - Building and pushing the images
Create a build template to configure how to build your container images.
Create the build.yaml template file.

-------------------------------------------------------------------
1.................................................................... 
oc process -f build.yml -p APP_NAME=javaee-cafe | oc create -f -

oc get all -l name=javaee-cafe

2....................................................................
oc start-build javaee-cafe-buildconfig --from-dir=.

oc get builds

oc logs build/javaee-cafe-buildconfig-1

3....................................................................
oc get imagestreams

Copiar ruta imagen y pegar en deploy

image-registry.openshift-image-registry.svc:5000/banco-poc/javaee-cafe-imagestream          


oc describe imagestream/javaee-cafe-imagestream

-------------------------------------------------------------------
b. Create secret

oc apply -f db-secret-postgresql.yaml
-------------------------------------------------------------------
c. Deploying the images
You can configure the specifics of the Open Liberty Operator-controlled deployment with a YAML configuration file.

Create the deploy.yml configuration file.

-------------------------------------------------------------------
4....................................................................
Cambiar images
oc apply -f deploy_openshift.yml

oc get OpenLibertyApplications

oc describe olapps/javaee-cafe-simple

oc get routes

----------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------

mvn clean install
mvn clean install -Ddb.server.name=localhost -Ddb.port.number=5432 -Ddb.name=banco -Ddb.user=userlp -Ddb.password=userlp12345 liberty:dev

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
podman build -t javaee-cafe-simple:1.0.0 --pull .

podman run -it --rm -p 9080:9080 -e DB_SERVER_NAME=10.20.30.39 -e DB_PORT_NUMBER=5432 -e DB_NAME=banco -e DB_USER=userlp -e DB_PASSWORD=userlp12345 javaee-cafe-simple:1.0.0

podman build -t javaee-cafe-simple:1.0.0 --pull .
podman run -it --rm -p 9080:9080 javaee-cafe-simple:1.0.0

http://10.10.0.172:9080/rest/enqueue?msg=somemessage
---------------------------------------------------------------------------------------------------------------------------------------

