FROM openliberty/open-liberty:kernel-java8-openj9-ubi

ARG VERSION=1.0
ARG REVISION=SNAPSHOT

COPY --chown=1001:0 src/main/liberty/config /config/
COPY --chown=1001:0 target/javaee-cafe.war /config/apps/
COPY --chown=1001:0 target/javaee-cafe/WEB-INF/lib/postgresql-42.2.4.jar /opt/ol/wlp/usr/shared/resources/

RUN configure.sh